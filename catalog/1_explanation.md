## Information

- This directory(application) is created by command : `python manage.py startapp catalog`

- The tool creates a new folder and populates it with files for the different parts of the application. Most of the files are named after their purpose e.g. 
  - views should be stored in **views.py**
  - models in **models.py**
  - tests in **tests.py**
  - administration site configuration in **admin.py**
  - application registration in **apps.py**
  - and contain some minimal boilerplate code for working with the associated objects

- A migrations folder, used to store "migrations" — files that allow you to automatically update your database as you modify your models.
- __init__.py — an empty file created here so that Django/Python will recognize the folder as a Python Package and allow you to use its objects within other parts of the project.


## Registering the catalog application

Now that the application has been created, we have to register it with the project so that it will be included when any tools are run (like adding models to the database for example). Applications are registered by adding them to the `INSTALLED_APPS` list in the project settings.