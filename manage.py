#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""

""" 
Explanation for manage.py file

The manage.py script is used to create applications, work with databases, and start the development web server.
"""

import os
import sys


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'locallibrary.settings')
    """ 
    Explanation for line above
    
    os.environ: This part accesses the environment variables in Python through the os module. Environment variables are essentially dynamic-named values that affect the way processes run on a computer.
    
    .setdefault('DJANGO_SETTINGS_MODULE', 'mytestsite.settings'): This method sets the value of an environment variable named 'DJANGO_SETTINGS_MODULE' to 'mytestsite.settings' if it's not already set.
    
    'DJANGO_SETTINGS_MODULE' is a special environment variable used by Django to determine which settings file to use. This file contains various configurations for your Django project, such as database settings, static file paths, and more.
    
    'mytestsite.settings' refers to the Python module (settings.py) where your Django project's settings are defined. This is a typical naming convention for Django projects: the project's settings file is usually located in a module named settings within the project directory.
    """
    
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
